import createClient from 'openapi-fetch'

import type { paths } from '~/lib/api/v1' // (generated from openapi-typescript)

type CallbackFunction = () => void
type CallbackFunctionVariadic = (...args: any) => any

export default () => {
  const runtimeConfig = useRuntimeConfig()

  const loadingCount = ref(0)
  const isLoading = computed(() => loadingCount.value === 0)

  function wrap<T extends CallbackFunction>(originalFn: CallbackFunctionVariadic): T {
    const wrapFn = async (...args: any) => {
      try {
        // console.log('loading')
        loadingCount.value += 1
        const res = await originalFn(...args)
        const rawResponse = res.response as Response
        if (rawResponse.status === 403 || rawResponse.status === 401) {
          // Logout
          console.warn(`Push login because rawResponse.status ${rawResponse.status}`)
        }
        if (res.error)
          throw res.error
        return res.data
      }
      finally {
        loadingCount.value -= 1
      }
    }
    return wrapFn as unknown as T
  }

  const { GET, POST, PUT, PATCH, DELETE } = createClient<paths>({
    baseUrl: runtimeConfig.public.apiBaseUrl as string,
    credentials: 'include',
    headers: {
      'x-original-source': 'AT_YC',
    },
  })
  return {
    loadingCount,
    isLoading,
    get: wrap(GET),
    post: wrap(POST),
    put: wrap(PUT),
    patch: wrap(PATCH),
    delete: wrap(DELETE),
  }
}

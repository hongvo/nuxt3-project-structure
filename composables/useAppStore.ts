import { defineStore } from 'pinia'

interface State {
  id: number | null
}

export const useAppStore = defineStore('app', {
  state: (): State => ({
    id: null,
  }),

  actions: {
    setId(id: number) {
      this.id = id
    },
  },
})

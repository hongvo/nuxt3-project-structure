// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  ssr: false,
  css: ['@/assets/scss/main.scss'],
  modules: ['@nuxtjs/tailwindcss', '@pinia/nuxt', '@vueuse/nuxt', '@nuxtjs/i18n', '@nuxtjs/svg-sprite'],
  i18n: {
    vueI18n: './i18n.config.ts', // if you are using custom path, default
  },
  svgSprite: {
    input: '~/assets/v1/sprite/svg',
    output: '~/assets/v1/sprite/gen',
  },
  vite: {
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: '@use "@/assets/scss/variables.scss" as *;',
        },
      },
    },
  },
  runtimeConfig: {
    public: {
      apiBaseUrl: process.env.MT_API_URL || 'https://api-at.dev.ycomm.work',
      corsBaseUrl: process.env.CORS_URL || 'https://cors-at.dev.ycomm.work',
      kratosBaseUrl: process.env.PUB_KRATOS_PUBLIC_ENDPOINT || 'https://kratos-at.dev.ycomm.work',
    },
  },
})

/**
 * This file was auto-generated by openapi-typescript.
 * Do not make direct changes to the file.
 */


export interface paths {
  "/api-v1/auth/admin/group": {
    put: operations["updateGroup"];
  };
  "/api-v1/auth/admin/identities": {
    put: operations["adminUpdatePassword"];
    post: operations["createIdentities"];
    delete: operations["deleteIdentities"];
  };
  "/api-v1/auth/identities": {
    put: operations["userUpdatePassword"];
  };
  "/api-v1/auth/info": {
    get: operations["getUserInfo"];
  };
  "/api-v1/auth/login-failed": {
    post: operations["saveLoginFailedLog"];
  };
  "/api-v1/auth/logout": {
    post: operations["saveLogoutLog"];
  };
  "/api-v1/proxy/execute": {
    post: operations["execute"];
  };
  "/api-v1/proxy/healthcheck": {
    get: operations["healthcheck"];
  };
  "/api-v1/work/kratos/webhook/logging-identity": {
    post: operations["afterLogin"];
  };
  "/api-v1/work/kratos/webhook/reject": {
    post: operations["reject"];
  };
  "/api-v1/work/logs/action": {
    post: operations["getActionLogs"];
  };
  "/api-v1/work/logs/login": {
    post: operations["getLoginLogs"];
  };
  "/api-v2/migration/image": {
    get: operations["migrateFromImageTable"];
  };
  "/api-v2/migration/image-format": {
    get: operations["migrateImageFormat"];
  };
  "/api-v2/migration/rollback": {
    get: operations["rollback"];
  };
  "/api-v2/work/chapters": {
    post: operations["createDraftChapter"];
  };
  "/api-v2/work/chapters/": {
    delete: operations["deleteChapter"];
  };
  "/api-v2/work/chapters/{id}": {
    get: operations["getChapter"];
    put: operations["updateChapter"];
  };
  "/api-v2/work/chapters/{id}/export-draft-excel": {
    post: operations["exportDraftExcel"];
  };
  "/api-v2/work/chapters/{id}/export-draft-image": {
    post: operations["exportDraftImage"];
  };
  "/api-v2/work/chapters/{id}/export-draft-json": {
    post: operations["exportDraftJSON"];
  };
  "/api-v2/work/chapters/{id}/export-draft-psd": {
    post: operations["exportDraftPSD"];
  };
  "/api-v2/work/chapters/{id}/export-excel": {
    get: operations["exportExcel"];
  };
  "/api-v2/work/chapters/{id}/export-image": {
    get: operations["exportImage"];
  };
  "/api-v2/work/chapters/{id}/export-json": {
    get: operations["exportJson"];
  };
  "/api-v2/work/chapters/{id}/export-psd": {
    get: operations["exportPsd"];
  };
  "/api-v2/work/chapters/{id}/text-boxes": {
    post: operations["createTextBoxes"];
  };
  "/api-v2/work/chapters/get-training-chapters": {
    get: operations["exportTrainingJSON"];
  };
  "/api-v2/work/chapters/search": {
    get: operations["searchChapter"];
  };
  "/api-v2/work/crawl": {
    get: operations["getListCrawlSite"];
  };
  "/api-v2/work/ocr/engines": {
    get: operations["getEngineList_1"];
  };
  "/api-v2/work/ocr/google-vision": {
    post: operations["detectByGoogleVision"];
  };
  "/api-v2/work/ocr/ycomm-vision": {
    post: operations["detectByYcommVision"];
  };
  "/api-v2/work/translate": {
    post: operations["translate"];
  };
  "/api-v2/work/translate/engines": {
    get: operations["getEngineList"];
  };
  "/api-v2/work/translate/locales": {
    get: operations["getLocaleList"];
  };
  "/api-v2/work/translate/target-locales": {
    get: operations["getTranslateLocaleList"];
  };
  "/api-v2/work/translate/target-locales-pts": {
    get: operations["getTranslateLocaleListForPts"];
  };
  "/api-v2/work/translate/translate-pts": {
    post: operations["translatePts"];
  };
  "/api/work/CREATE_MANUAL_LOG": {
    post: operations["createManualLog"];
  };
  "/api/work/CREATE_UPDATE_CHAPTER": {
    post: operations["CreateUpdateChapter"];
  };
  "/api/work/DELETE_LIST_CHAPTER": {
    delete: operations["deleteChapters"];
  };
  "/api/work/GET_CHAPTER_DETAIL": {
    post: operations["GetChapterDetail"];
  };
  "/api/work/GET_LIST_CHAPTER": {
    post: operations["GetListChapter"];
  };
  "/api/work/image/export": {
    post: operations["exportImages"];
  };
  "/api/work/RENAME_COMIC_OR_CHAPTER": {
    post: operations["RenameComicOrChapter"];
  };
  "/api/work/s3/downloadImages": {
    post: operations["downloadImage"];
  };
  "/api/work/s3/getS3Url": {
    get: operations["getS3Url"];
  };
  "/api/work/s3/preSignedUpload": {
    post: operations["preSignedUpload"];
  };
}

export type webhooks = Record<string, never>;

export interface components {
  schemas: {
    AdminUpdateIdentityPasswordReq: {
      EMAIL?: string;
      PASSWORD?: string;
    };
    AppResponseChapterDetailRes: {
      DATA?: components["schemas"]["ChapterDetailRes"];
      ERR_CODE?: string;
      REASON?: string;
      RESULT?: string;
    };
    AppResponseCreateUpdateChapterRes: {
      DATA?: components["schemas"]["CreateUpdateChapterRes"];
      ERR_CODE?: string;
      REASON?: string;
      RESULT?: string;
    };
    AppResponseExportTrainingResult: {
      DATA?: components["schemas"]["ExportTrainingResult"];
      ERR_CODE?: string;
      REASON?: string;
      RESULT?: string;
    };
    AppResponseGetChapterDetailRes: {
      DATA?: components["schemas"]["GetChapterDetailRes"];
      ERR_CODE?: string;
      REASON?: string;
      RESULT?: string;
    };
    AppResponseListCrawlSiteDto: {
      DATA?: components["schemas"]["CrawlSiteDto"][];
      ERR_CODE?: string;
      REASON?: string;
      RESULT?: string;
    };
    AppResponseListLocale: {
      DATA?: components["schemas"]["Locale"][];
      ERR_CODE?: string;
      REASON?: string;
      RESULT?: string;
    };
    AppResponseListOCREngineEntity: {
      DATA?: components["schemas"]["OCREngineEntity"][];
      ERR_CODE?: string;
      REASON?: string;
      RESULT?: string;
    };
    AppResponseListString: {
      DATA?: string[];
      ERR_CODE?: string;
      REASON?: string;
      RESULT?: string;
    };
    AppResponseListTargetLocale: {
      DATA?: components["schemas"]["TargetLocale"][];
      ERR_CODE?: string;
      REASON?: string;
      RESULT?: string;
    };
    AppResponseListTextBoxDtoRes: {
      DATA?: components["schemas"]["TextBoxDtoRes"][];
      ERR_CODE?: string;
      REASON?: string;
      RESULT?: string;
    };
    AppResponseListTranslateEngineEntity: {
      DATA?: components["schemas"]["TranslateEngineEntity"][];
      ERR_CODE?: string;
      REASON?: string;
      RESULT?: string;
    };
    AppResponseObject: {
      DATA?: Record<string, never>;
      ERR_CODE?: string;
      REASON?: string;
      RESULT?: string;
    };
    AppResponseRes: {
      DATA?: components["schemas"]["Res"];
      ERR_CODE?: string;
      REASON?: string;
      RESULT?: string;
    };
    AppResponseResponse: {
      DATA?: components["schemas"]["Response"];
      ERR_CODE?: string;
      REASON?: string;
      RESULT?: string;
    };
    AppResponseSearchRes: {
      DATA?: components["schemas"]["SearchRes"];
      ERR_CODE?: string;
      REASON?: string;
      RESULT?: string;
    };
    AppResponseTranslatePtsRes: {
      DATA?: components["schemas"]["TranslatePtsRes"];
      ERR_CODE?: string;
      REASON?: string;
      RESULT?: string;
    };
    AppResponseTranslateRes: {
      DATA?: components["schemas"]["TranslateRes"];
      ERR_CODE?: string;
      REASON?: string;
      RESULT?: string;
    };
    BasePermissionsRes: {
      CAN_CRAWL?: boolean;
      CAN_CRAWL_COMICO?: boolean;
      CAN_CRAWL_KILEDEL?: boolean;
      CAN_OCR?: boolean;
      CAN_TRANSLATE_API?: boolean;
    };
    ChapterDetailRes: {
      CHAPTER_NAME?: string;
      CHAPTER_SITE_ID?: string;
      COMIC?: components["schemas"]["Comic"];
      CREATED_AT?: string;
      DEFAULT_TRANSLATED_LOCALE_CODE?: string;
      FROM_SITE?: string;
      /** Format: int64 */
      ID?: number;
      IMAGE_URLS?: string[];
      LOCALES?: components["schemas"]["Locale"][];
      /** Format: int32 */
      OCR_DETECTED_TEXT_BOX_COUNT?: number;
      ORIGINAL_LOCALE_CODE?: string;
      OWNER?: string;
      PERMISSION?: components["schemas"]["ChapterPermissionRes"];
      /** Format: int32 */
      TRANSLATED_TEXT_BOX_COUNT?: number;
      UPDATED_AT?: string;
      UPLOAD?: boolean;
      USER_GROUP?: string;
    };
    ChapterPermissionRes: {
      CAN_TRANSLATE?: boolean;
      EXPORT_EXCEL?: boolean;
      EXPORT_IMAGE?: boolean;
      EXPORT_JSON?: boolean;
      EXPORT_PSD?: boolean;
      SHOW_DRAW_TEXT_BOX_BTN?: boolean;
      SHOW_TOGGLE_TEXT_BOX_BTN?: boolean;
      SHOW_TOGGLE_VISION_BOX_BTN?: boolean;
      SHOW_TRANSLATE_BTN?: boolean;
      SHOW_TRANSLATE_ENGINE_BTN?: boolean;
    };
    Comic: {
      COMIC_SITE_ID?: string;
      /** Format: int64 */
      ID?: number;
      NAME?: string;
    };
    ContentTrans: {
      CREATED_AT?: string;
      FILE_ZIP?: string;
      FILES?: components["schemas"]["FileInfo"][];
      IMAGES?: components["schemas"]["ImgTranslated"][];
      JSON?: string;
      LANG?: string;
      /** Format: int64 */
      LANG_ID?: number;
      ORIGINAL?: boolean;
      /** Format: int32 */
      TEXT_NUM?: number;
      UPDATED_AT?: string;
    };
    CrawlSiteDto: {
      /** Format: int32 */
      CODE?: number;
      OCR?: boolean;
      SITE_NAME?: string;
      TRANSLATE?: boolean;
      URL?: string;
      VERSION?: string;
    };
    CreateDefaultTextBoxesReq: {
      ORIGINAL_LOCALE_CODE?: string;
      TEXT_BOXES?: components["schemas"]["TextBoxDtoReq"][];
      TRANSLATED_LOCALE_CODE?: string;
    };
    CreateEmptyChapterReq: {
      FROM_SITE?: string;
      UPLOAD?: boolean;
    };
    CreateIdentityReq: {
      EMAIL?: string;
      GROUP?: string;
      PASSWORD?: string;
      PS_FLAG?: boolean;
      ROLE?: string;
    };
    CreateUpdateChapterDto: {
      CHAPTER_NAME?: string;
      CHAPTER_SITE_ID?: string;
      /** Format: int64 */
      COMIC_ID?: number;
      COMIC_NAME?: string;
      COMIC_SITE_ID?: string;
      CRAWLER_DATA?: Record<string, never>;
      /** Format: int64 */
      ID?: number;
      /** Format: int32 */
      IMAGE_COUNT?: number;
      /** Format: int64 */
      MY_SITE_ID?: number;
      ORIGIN?: components["schemas"]["Language"];
      ORIGINAL_LOCALE?: string;
      TRANSLATES?: components["schemas"]["Language"][];
      UPLOAD?: boolean;
    };
    CreateUpdateChapterRes: {
      /** Format: int64 */
      CHAPTER_ID?: number;
      /** Format: int64 */
      COMIC_ID?: number;
      PERMISSION?: components["schemas"]["ChapterPermissionRes"];
    };
    CreateUpdateTextBoxesRequest: {
      CHAPTER_NAME?: string;
      COMIC_NAME?: string;
      COMIC_SITE_ID?: string;
      /** Format: int64 */
      MY_SITE_ID?: number;
      ORIGINAL_LOCALE_CODE?: string;
      ORIGINAL_TEXT_BOXES?: components["schemas"]["TextBoxDtoReq"][];
      TRANSLATED_LOCALE_CODE?: string;
      TRANSLATED_TEXT_BOXES?: components["schemas"]["TextBoxDtoReq"][];
      UPLOAD?: boolean;
    };
    CropImageInfoDto: {
      FILE_NAME?: string;
      /** Format: int32 */
      HEIGHT?: number;
      /** Format: int32 */
      WIDTH?: number;
    };
    DownloadImageReq: {
      CHAPTER_NAME?: string;
      COMIC_NAME?: string;
      URLS?: string[];
    };
    ExportDraftChapterRequest: {
      CHAPTER_NAME?: string;
      COMIC_NAME?: string;
      TARGET_LOCALE_CODE?: string;
      TRANSLATED_TEXT_BOXES?: components["schemas"]["TextBoxDtoReq"][];
      UPLOAD?: boolean;
    };
    ExportImageReq: {
      CROP_IMAGE_INFOS?: components["schemas"]["CropImageInfoDto"][];
      EXPORT_NAME?: string;
      IMAGES?: components["schemas"]["MergeImageInfoDto"][];
      /** Format: int32 */
      MAX_HEIGHT?: number;
      /** Format: int32 */
      MAX_WIDTH?: number;
    };
    ExportTrainingResult: {
      ITEMS?: components["schemas"]["TrainingChapter"][];
      /** Format: int32 */
      PAGE?: number;
      /** Format: int32 */
      PER_PAGE?: number;
      /** Format: int64 */
      TOTAL_ITEMS?: number;
      /** Format: int32 */
      TOTAL_PAGES?: number;
    };
    FileInfo: {
      CREATED_AT?: string;
      /** Format: int64 */
      FILE_ID?: number;
      UPDATED_AT?: string;
      URL?: string;
    };
    GenPreSignedUrlReq: {
      /** Format: int64 */
      CONTENT_LENGTH?: number;
      CONTENT_TYPE?: string;
      FILE_NAME?: string;
    };
    GetChapterDetailReq: {
      /** Format: int64 */
      ID?: number;
      TRANSLATE?: string;
    };
    GetChapterDetailRes: {
      CHAPTER_NAME?: string;
      /** Format: int64 */
      COMIC_ID?: number;
      COMIC_NAME?: string;
      CRAWLER_DATA?: Record<string, never>;
      CREATED_AT?: string;
      /** Format: int64 */
      ID?: number;
      LIST_LANG?: components["schemas"]["ListLang"][];
      SITE?: string;
      TRANSLATED?: components["schemas"]["ContentTrans"][];
      UPDATED_AT?: string;
    };
    GetListChapterReq: {
      /** Format: int64 */
      CHAPTER_ID?: number;
      CHAPTER_NAME?: string;
      /** Format: int64 */
      COMIC_ID?: number;
      COMIC_NAME?: string;
      CREATED_TIME?: string;
      UPDATED_TIME?: string;
    };
    GoogleVisionReq: {
      /** Format: int64 */
      CHAPTER_ID?: number;
      /** Format: int32 */
      GETTING_TYPE_ID?: number;
      IMAGE?: components["schemas"]["Image"][];
      IMAGES_IN_BASE64?: string[];
      VISION_REQUEST?: {
        [key: string]: Record<string, never>;
      };
    };
    Image: {
      /** Format: int64 */
      ID?: number;
      /** Format: int32 */
      IMG_NO?: number;
      JSON?: Record<string, never>;
      OBJECT_KEY?: string;
    };
    ImgTranslated: {
      CREATED_AT?: string;
      /** Format: int64 */
      IMG_ID?: number;
      /** Format: int32 */
      IMG_NO?: number;
      JSON?: Record<string, never>;
      UPDATED_AT?: string;
      URL?: string;
    };
    Language: {
      /** Format: int64 */
      ID?: number;
      IMAGES?: components["schemas"]["Image"][];
      LOCALE?: string;
      /** Format: int32 */
      TEXT_NUMBER?: number;
    };
    ListLang: {
      LANG?: string;
      /** Format: int64 */
      LANG_ID?: number;
      /** Format: int32 */
      TEXT_NUMBER?: number;
    };
    Locale: {
      LOCALE_CODE?: string;
      ORIGINAL?: boolean;
      TEXT_BOXES?: components["schemas"]["TextBoxDtoRes"][];
    };
    LogReq: {
      ACCOUNT?: string;
      /** Format: int32 */
      ACTION_TYPE?: number;
      CHAPTER_NAME?: string;
      COMIC_NAME?: string;
      /** Format: int32 */
      ERROR_CODE?: number;
      /** Format: int32 */
      GETTING_TYPE?: number;
      /** Format: int32 */
      IMAGE_COUNT?: number;
      IMAGE_NAME?: string;
      /** Format: int32 */
      RESULT?: number;
      /** Format: int32 */
      TRANSLATED_CHAR_COUNT?: number;
      /** Format: int32 */
      TRANSLATED_TEXT_COUNT?: number;
    };
    MergeImageInfoDto: {
      /** Format: int32 */
      HEIGHT?: number;
      OBJECT_KEY?: string;
      /** Format: int32 */
      OFFSET_TOP?: number;
      /** Format: int32 */
      WIDTH?: number;
    };
    OCREngineEntity: {
      CREATED_AT?: string;
      /** Format: int64 */
      ID?: number;
      NAME?: string;
      UPDATED_AT?: string;
    };
    PaginationReq: {
      /** Format: int32 */
      PAGE?: number;
      /** Format: int32 */
      PER_PAGE?: number;
    };
    RenameReq: {
      /** Format: int64 */
      CHAPTER_ID?: number;
      CHAPTER_RENAME?: string;
      /** Format: int64 */
      COMIC_ID?: number;
      COMIC_RENAME?: string;
    };
    Req: {
      CHAPTER_IDS?: number[];
    };
    Request: {
      CAN_CRAWL_COMICO?: boolean;
      CAN_CRAWL_KILEDEL?: boolean;
      /** Format: int64 */
      GROUP_ID?: number;
      /** Format: int32 */
      LIMIT_COUNT?: number;
      LIMITED?: boolean;
    };
    Res: {
      MESSAGE?: string;
    };
    Response: {
      MESSAGE?: string;
    };
    SearchReqGetListChapterReq: {
      CONDITIONS: components["schemas"]["GetListChapterReq"];
      /** Format: int32 */
      SEARCH_PAGE?: number;
      /** Format: int32 */
      SEARCH_UNIT?: number;
    };
    SearchRes: {
      /** Format: int32 */
      CURRENT_PAGE?: number;
      ITEMS?: components["schemas"]["SearchResItem"][];
      /** Format: int32 */
      SIZE?: number;
      /** Format: int32 */
      TOTAL_PAGES?: number;
      /** Format: int64 */
      TOTAL_RESULT?: number;
    };
    SearchResItem: {
      /** Format: int64 */
      CHAPTER_ID?: number;
      CHAPTER_NAME?: string;
      /** Format: int64 */
      COMIC_ID?: number;
      COMIC_NAME?: string;
      CREATED_TIME?: string;
      /** Format: int64 */
      NO?: number;
      /** Format: int32 */
      ORIGINAL_TEXT_BOXES?: number;
      TRANSLATE_TEX_BOX_NAME?: string;
      /** Format: int32 */
      TRANSLATE_TEX_BOXES_COUNT?: number;
      UPDATED_TIME?: string;
    };
    TargetLocale: {
      CODE?: string;
      DEFAULT?: boolean;
      /** Format: int64 */
      ID?: number;
      ISO2?: string;
      NAME?: string;
    };
    TextBoxDtoReq: {
      BACKGROUND_COLOR?: string;
      CAN_EDIT_BACKGROUND?: boolean;
      /** Format: int32 */
      COMBINED_X?: number;
      /** Format: int32 */
      COMBINED_Y?: number;
      FEEDBACK?: boolean;
      FONT_FAMILY?: string;
      FONT_SIZE?: string;
      FONT_STYLE?: string;
      FONT_WEIGHT?: string;
      /** Format: int32 */
      HEIGHT?: number;
      /** Format: int64 */
      ID?: number;
      LOCALE_CODE?: string;
      /** Format: int32 */
      ORIGINAL_TEXT_BOX_ID?: number;
      /** Format: int32 */
      ROTATE_DEGREE?: number;
      TEXT?: string;
      TEXT_ALIGN?: string;
      TEXT_COLOR?: string;
      TEXT_DECORATION?: string;
      TEXT_DECORATION_COLOR?: string;
      TEXT_SHADOW?: string;
      TEXT_UNDERLINE_OFFSET?: string;
      /** Format: int32 */
      WIDTH?: number;
    };
    TextBoxDtoRes: {
      BACKGROUND_COLOR?: string;
      CAN_EDIT_BACKGROUND?: boolean;
      /** Format: int64 */
      CHAPTER_ID?: number;
      /** Format: int32 */
      COMBINED_X?: number;
      /** Format: int32 */
      COMBINED_Y?: number;
      CREATED_AT?: string;
      FEEDBACK?: boolean;
      /** Format: int64 */
      FIRST_TRANSLATE_ENGINE_ID?: number;
      FONT_FAMILY?: string;
      FONT_SIZE?: string;
      FONT_STYLE?: string;
      FONT_WEIGHT?: string;
      /** Format: int32 */
      HEIGHT?: number;
      /** Format: int64 */
      ID?: number;
      LOCALE_CODE?: string;
      /** Format: int64 */
      ORIGINAL_TEXT_BOX_ID?: number;
      PLACE_HOLDER?: string;
      /** Format: int32 */
      ROTATE_DEGREE?: number;
      TEXT?: string;
      TEXT_ALIGN?: string;
      TEXT_COLOR?: string;
      TEXT_DECORATION?: string;
      TEXT_DECORATION_COLOR?: string;
      TEXT_SHADOW?: string;
      TEXT_UNDERLINE_OFFSET?: string;
      UPDATED_AT?: string;
      VISION_BOX?: components["schemas"]["VisionBoxDtoRes"];
      /** Format: int32 */
      WIDTH?: number;
    };
    TrainingChapter: {
      /** Format: int64 */
      CHAPTER_ID?: number;
      CHAPTER_NAME?: string;
      COMIC_NAME?: string;
      IMAGES?: components["schemas"]["TrainingImage"][];
      LOCALE_CODE?: string;
    };
    TrainingImage: {
      /** Format: int64 */
      HEIGHT?: number;
      NAME?: string;
      URL?: string;
      VISION_BOXES?: components["schemas"]["TrainingVisionBox"][];
      /** Format: int64 */
      WIDTH?: number;
    };
    TrainingVisionBox: {
      /** Format: int32 */
      COMBINED_X?: number;
      /** Format: int32 */
      COMBINED_Y?: number;
      /** Format: int32 */
      HEIGHT?: number;
      TEXT?: string;
      /** Format: int32 */
      WIDTH?: number;
    };
    TranslatedText: {
      ORIGINAL_TEXT?: string;
      TRANSLATED_TEXT?: string;
    };
    TranslateEngineEntity: {
      /** Format: int64 */
      CODE?: number;
      CREATED_AT?: string;
      /** Format: int64 */
      ID?: number;
      NAME?: string;
      UPDATED_AT?: string;
    };
    TranslatePtsReq: {
      SOURCE_LOCALE_CODE?: string;
      TARGET_LOCALE_CODE?: string;
      TEXT_LIST?: string[];
    };
    TranslatePtsRes: {
      SOURCE_LOCALE_CODE?: string;
      TARGET_LOCALE_CODE?: string;
      TRANSLATED_TEXT_LIST?: components["schemas"]["TranslatedText"][];
    };
    TranslateReq: {
      /** Format: int64 */
      CHAPTER_ID?: number;
      /** Format: int32 */
      GETTING_TYPE_ID?: number;
      /** Format: int64 */
      TRANSLATE_ENGINE_CODE?: number;
      /** Format: int64 */
      TRANSLATE_LOCALE_ID?: number;
    };
    TranslateRes: {
      SOURCE_LOCALE_CODE?: string;
      TARGET_LOCALE_CODE?: string;
      TRANSLATED_TEXT_LIST?: components["schemas"]["TranslateTextList"][];
    };
    TranslateTextList: {
      ORIGINAL_TEXT?: string;
      /** Format: int32 */
      ORIGINAL_TEXT_BOX_ID?: number;
      TRANSLATED_TEXT?: string;
      /** Format: int32 */
      TRANSLATED_TEXT_BOX_ID?: number;
    };
    UserInfoRes: {
      /** Format: int64 */
      CURRENT_USING_COUNT?: number;
      GROUP?: string;
      /** Format: int64 */
      LIMIT_COUNT?: number;
      LIMITED?: boolean;
      PERMISSION?: components["schemas"]["BasePermissionsRes"];
      PS_FLAG?: boolean;
      ROLE?: string;
    };
    UserUpdateIdentityPasswordReq: {
      PASSWORD?: string;
    };
    VisionBoxDtoRes: {
      /** Format: int32 */
      COMBINED_X?: number;
      /** Format: int32 */
      COMBINED_Y?: number;
      DOMINANT_COLOR?: string;
      /** Format: int32 */
      HEIGHT?: number;
      /** Format: int64 */
      ID?: number;
      /** Format: int32 */
      WIDTH?: number;
    };
  };
  responses: never;
  parameters: never;
  requestBodies: never;
  headers: never;
  pathItems: never;
}

export type $defs = Record<string, never>;

export type external = Record<string, never>;

export interface operations {

  updateGroup: {
    requestBody: {
      content: {
        "application/json": components["schemas"]["Request"];
      };
    };
    responses: {
      /** @description OK */
      200: {
        content: {
          "application/json": components["schemas"]["AppResponseResponse"];
        };
      };
    };
  };
  adminUpdatePassword: {
    requestBody: {
      content: {
        "application/json": components["schemas"]["AdminUpdateIdentityPasswordReq"];
      };
    };
    responses: {
      /** @description OK */
      200: {
        content: {
          "application/json": Record<string, never>;
        };
      };
    };
  };
  createIdentities: {
    requestBody: {
      content: {
        "application/json": components["schemas"]["CreateIdentityReq"];
      };
    };
    responses: {
      /** @description OK */
      200: {
        content: {
          "application/json": Record<string, never>;
        };
      };
    };
  };
  deleteIdentities: {
    requestBody: {
      content: {
        "application/json": components["schemas"]["Req"];
      };
    };
    responses: {
      /** @description OK */
      200: {
        content: {
          "application/json": components["schemas"]["AppResponseRes"];
        };
      };
    };
  };
  userUpdatePassword: {
    requestBody: {
      content: {
        "application/json": components["schemas"]["UserUpdateIdentityPasswordReq"];
      };
    };
    responses: {
      /** @description OK */
      200: {
        content: {
          "application/json": Record<string, never>;
        };
      };
    };
  };
  getUserInfo: {
    responses: {
      /** @description OK */
      200: {
        content: {
          "application/json": components["schemas"]["UserInfoRes"];
        };
      };
    };
  };
  saveLoginFailedLog: {
    requestBody: {
      content: {
        "application/json": components["schemas"]["LogReq"];
      };
    };
    responses: {
      /** @description OK */
      200: {
        content: never;
      };
    };
  };
  saveLogoutLog: {
    requestBody: {
      content: {
        "application/json": components["schemas"]["LogReq"];
      };
    };
    responses: {
      /** @description OK */
      200: {
        content: never;
      };
    };
  };
  execute: {
    parameters: {
      query: {
        url: string;
      };
    };
    responses: {
      /** @description OK */
      200: {
        content: never;
      };
    };
  };
  healthcheck: {
    responses: {
      /** @description OK */
      200: {
        content: {
          "application/json": string;
        };
      };
    };
  };
  afterLogin: {
    responses: {
      /** @description OK */
      200: {
        content: {
          "application/json": string;
        };
      };
    };
  };
  reject: {
    responses: {
      /** @description OK */
      200: {
        content: {
          "application/json": string;
        };
      };
    };
  };
  getActionLogs: {
    requestBody: {
      content: {
        "application/json": components["schemas"]["PaginationReq"];
      };
    };
    responses: {
      /** @description OK */
      200: {
        content: {
          "application/json": Record<string, never>;
        };
      };
    };
  };
  getLoginLogs: {
    requestBody: {
      content: {
        "application/json": components["schemas"]["PaginationReq"];
      };
    };
    responses: {
      /** @description OK */
      200: {
        content: {
          "application/json": Record<string, never>;
        };
      };
    };
  };
  migrateFromImageTable: {
    parameters: {
      query?: {
        chapterId?: number;
      };
    };
    responses: {
      /** @description OK */
      200: {
        content: {
          "application/json": components["schemas"]["AppResponseRes"];
        };
      };
    };
  };
  migrateImageFormat: {
    responses: {
      /** @description OK */
      200: {
        content: {
          "application/json": components["schemas"]["AppResponseRes"];
        };
      };
    };
  };
  rollback: {
    parameters: {
      query?: {
        chapterId?: number;
      };
    };
    responses: {
      /** @description OK */
      200: {
        content: {
          "application/json": components["schemas"]["AppResponseRes"];
        };
      };
    };
  };
  createDraftChapter: {
    requestBody: {
      content: {
        "application/json": components["schemas"]["CreateEmptyChapterReq"];
      };
    };
    responses: {
      /** @description OK */
      200: {
        content: {
          "application/json": components["schemas"]["AppResponseCreateUpdateChapterRes"];
        };
      };
    };
  };
  deleteChapter: {
    requestBody: {
      content: {
        "application/json": components["schemas"]["Req"];
      };
    };
    responses: {
      /** @description OK */
      200: {
        content: {
          "application/json": components["schemas"]["AppResponseRes"];
        };
      };
    };
  };
  getChapter: {
    parameters: {
      path: {
        id: number;
      };
    };
    responses: {
      /** @description OK */
      200: {
        content: {
          "application/json": components["schemas"]["AppResponseChapterDetailRes"];
        };
      };
    };
  };
  updateChapter: {
    parameters: {
      path: {
        id: number;
      };
    };
    requestBody: {
      content: {
        "application/json": components["schemas"]["CreateUpdateTextBoxesRequest"];
      };
    };
    responses: {
      /** @description OK */
      200: {
        content: {
          "application/json": components["schemas"]["AppResponseChapterDetailRes"];
        };
      };
    };
  };
  exportDraftExcel: {
    parameters: {
      path: {
        id: number;
      };
    };
    requestBody: {
      content: {
        "application/json": components["schemas"]["ExportDraftChapterRequest"];
      };
    };
    responses: {
      /** @description OK */
      200: {
        content: never;
      };
    };
  };
  exportDraftImage: {
    parameters: {
      path: {
        id: number;
      };
    };
    requestBody: {
      content: {
        "application/json": components["schemas"]["ExportDraftChapterRequest"];
      };
    };
    responses: {
      /** @description OK */
      200: {
        content: never;
      };
    };
  };
  exportDraftJSON: {
    parameters: {
      path: {
        id: number;
      };
    };
    requestBody: {
      content: {
        "application/json": components["schemas"]["ExportDraftChapterRequest"];
      };
    };
    responses: {
      /** @description OK */
      200: {
        content: never;
      };
    };
  };
  exportDraftPSD: {
    parameters: {
      path: {
        id: number;
      };
    };
    requestBody: {
      content: {
        "application/json": components["schemas"]["ExportDraftChapterRequest"];
      };
    };
    responses: {
      /** @description OK */
      200: {
        content: never;
      };
    };
  };
  exportExcel: {
    parameters: {
      query: {
        localeCode: string;
      };
      path: {
        id: number;
      };
    };
    responses: {
      /** @description OK */
      200: {
        content: never;
      };
    };
  };
  exportImage: {
    parameters: {
      query: {
        localeCode: string;
      };
      path: {
        id: number;
      };
    };
    responses: {
      /** @description OK */
      200: {
        content: never;
      };
    };
  };
  exportJson: {
    parameters: {
      query: {
        localeCode: string;
      };
      path: {
        id: number;
      };
    };
    responses: {
      /** @description OK */
      200: {
        content: never;
      };
    };
  };
  exportPsd: {
    parameters: {
      query: {
        localeCode: string;
      };
      path: {
        id: number;
      };
    };
    responses: {
      /** @description OK */
      200: {
        content: never;
      };
    };
  };
  createTextBoxes: {
    parameters: {
      path: {
        id: number;
      };
    };
    requestBody: {
      content: {
        "application/json": components["schemas"]["CreateDefaultTextBoxesReq"];
      };
    };
    responses: {
      /** @description OK */
      200: {
        content: {
          "application/json": components["schemas"]["AppResponseChapterDetailRes"];
        };
      };
    };
  };
  exportTrainingJSON: {
    parameters: {
      query: {
        page: number;
      };
    };
    responses: {
      /** @description OK */
      200: {
        content: {
          "application/json": components["schemas"]["AppResponseExportTrainingResult"];
        };
      };
    };
  };
  searchChapter: {
    parameters: {
      query?: {
        currentPage?: number;
        comicId?: string;
        comicName?: string;
        chapterId?: string;
        chapterName?: string;
        time?: string;
      };
    };
    responses: {
      /** @description OK */
      200: {
        content: {
          "application/json": components["schemas"]["AppResponseSearchRes"];
        };
      };
    };
  };
  getListCrawlSite: {
    responses: {
      /** @description OK */
      200: {
        content: {
          "application/json": components["schemas"]["AppResponseListCrawlSiteDto"];
        };
      };
    };
  };
  getEngineList_1: {
    responses: {
      /** @description OK */
      200: {
        content: {
          "application/json": components["schemas"]["AppResponseListOCREngineEntity"];
        };
      };
    };
  };
  detectByGoogleVision: {
    parameters: {
      query: {
        smartSplit: boolean;
      };
    };
    requestBody: {
      content: {
        "application/json": components["schemas"]["GoogleVisionReq"];
      };
    };
    responses: {
      /** @description OK */
      200: {
        content: {
          "application/json": components["schemas"]["AppResponseChapterDetailRes"];
        };
      };
    };
  };
  detectByYcommVision: {
    requestBody: {
      content: {
        "application/json": components["schemas"]["GoogleVisionReq"];
      };
    };
    responses: {
      /** @description OK */
      200: {
        content: {
          "application/json": components["schemas"]["AppResponseListTextBoxDtoRes"];
        };
      };
    };
  };
  translate: {
    requestBody: {
      content: {
        "application/json": components["schemas"]["TranslateReq"];
      };
    };
    responses: {
      /** @description OK */
      200: {
        content: {
          "application/json": components["schemas"]["AppResponseTranslateRes"];
        };
      };
    };
  };
  getEngineList: {
    responses: {
      /** @description OK */
      200: {
        content: {
          "application/json": components["schemas"]["AppResponseListTranslateEngineEntity"];
        };
      };
    };
  };
  getLocaleList: {
    responses: {
      /** @description OK */
      200: {
        content: {
          "application/json": components["schemas"]["AppResponseListLocale"];
        };
      };
    };
  };
  getTranslateLocaleList: {
    parameters: {
      query: {
        chapterId: number;
        translateEngineCode: number;
      };
    };
    responses: {
      /** @description OK */
      200: {
        content: {
          "application/json": components["schemas"]["AppResponseListTargetLocale"];
        };
      };
    };
  };
  getTranslateLocaleListForPts: {
    parameters: {
      query: {
        sourceLocaleCode: string;
        translateEngineCode: number;
      };
    };
    responses: {
      /** @description OK */
      200: {
        content: {
          "application/json": components["schemas"]["AppResponseListTargetLocale"];
        };
      };
    };
  };
  translatePts: {
    requestBody: {
      content: {
        "application/json": components["schemas"]["TranslatePtsReq"];
      };
    };
    responses: {
      /** @description OK */
      200: {
        content: {
          "application/json": components["schemas"]["AppResponseTranslatePtsRes"];
        };
      };
    };
  };
  createManualLog: {
    requestBody: {
      content: {
        "application/json": components["schemas"]["LogReq"];
      };
    };
    responses: {
      /** @description OK */
      200: {
        content: never;
      };
    };
  };
  CreateUpdateChapter: {
    requestBody: {
      content: {
        "application/json": components["schemas"]["CreateUpdateChapterDto"];
      };
    };
    responses: {
      /** @description OK */
      200: {
        content: {
          "application/json": components["schemas"]["AppResponseObject"];
        };
      };
    };
  };
  deleteChapters: {
    parameters: {
      query: {
        chapterIds: number[];
      };
    };
    responses: {
      /** @description OK */
      200: {
        content: {
          "application/json": components["schemas"]["AppResponseObject"];
        };
      };
    };
  };
  GetChapterDetail: {
    requestBody: {
      content: {
        "application/json": components["schemas"]["GetChapterDetailReq"];
      };
    };
    responses: {
      /** @description OK */
      200: {
        content: {
          "application/json": components["schemas"]["AppResponseGetChapterDetailRes"];
        };
      };
    };
  };
  GetListChapter: {
    requestBody: {
      content: {
        "application/json": components["schemas"]["SearchReqGetListChapterReq"];
      };
    };
    responses: {
      /** @description OK */
      200: {
        content: {
          "application/json": components["schemas"]["AppResponseObject"];
        };
      };
    };
  };
  exportImages: {
    requestBody: {
      content: {
        "application/json": components["schemas"]["ExportImageReq"];
      };
    };
    responses: {
      /** @description OK */
      200: {
        content: {
          "application/json": string;
        };
      };
    };
  };
  RenameComicOrChapter: {
    requestBody: {
      content: {
        "application/json": components["schemas"]["RenameReq"];
      };
    };
    responses: {
      /** @description OK */
      200: {
        content: {
          "application/json": components["schemas"]["AppResponseObject"];
        };
      };
    };
  };
  downloadImage: {
    requestBody: {
      content: {
        "application/json": components["schemas"]["DownloadImageReq"];
      };
    };
    responses: {
      /** @description OK */
      200: {
        content: {
          "application/json": components["schemas"]["AppResponseListString"];
        };
      };
    };
  };
  getS3Url: {
    parameters: {
      query: {
        objectKey: string;
      };
    };
    responses: {
      /** @description OK */
      200: {
        content: {
          "application/json": string;
        };
      };
    };
  };
  preSignedUpload: {
    requestBody: {
      content: {
        "application/json": components["schemas"]["GenPreSignedUrlReq"];
      };
    };
    responses: {
      /** @description OK */
      200: {
        content: {
          "application/json": components["schemas"]["AppResponseObject"];
        };
      };
    };
  };
}

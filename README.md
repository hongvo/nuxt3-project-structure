## Style guide
https://gitlab.com/ycommvn/handbook/-/blob/main/developer/frontend/vue3-style-guide.md?ref_type=heads

## Project structure
https://nuxt.com/docs/guide/directory-structure/nuxt


## Dependencies

```shell
yarn add @vueuse/nuxt @pinia/nuxt pinia @nuxtjs/i18n @nuxtjs/tailwindcss
yarn add eslint @antfu/eslint-config --dev // For eslint config
yarn add simple-git-hooks lint-staged --dev // Check lint pre-commit
yarn add openapi-typescript openapi-fetch // For fetch api with openapi
yarn add sass sass-loader --dev // For load sass
yarn add @vuepic/vue-datepicker
```

import en from '~/lib/locales/en.json'
import vi from '~/lib/locales/vi.json'

export default defineI18nConfig(() => {
  const messages = { en, vi }
  return {
    legacy: false,
    locale: 'en',
    messages,
  }
})
